<!DOCTYPE html>

<script src="https://code.jquery.com/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>



<script src="adduser.js"></script>

<script type="text/javascript">
    function changeScreenSize() {
        window.resizeTo(screen.width - 500, screen.height - 1000);
    }
</script>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
        <style>
            .modal-body {
                overflow-x: auto;

            }
        </style>
    </head>

    <body onload="changeScreenSize()">

        <div class="container">
            <form role="form" id="formadmin1" name="formadmin1"  method="POST"> 
                <div class="form-group">
                    <label class="col-md-4 control-label" for="username">Add User: </label>  
                    <div class="col-md-8">
                        <input id="username" name="username" placeholder="" class="form-control input-md" type="text">
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label" for="singlebutton"></label>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-success" name="send">Submit</button>
                        </div>
                    </div>
            </form>

        </div>
    </div>
    <div id="box-content"></div>

    <div class="container">


        <table class="table table-bordered table-hover" id="usertable">
            <thead>
                <tr>

                    <th>Users</th>
                    <th style=display:none;>EDIT</th>
                    <th style=display:none;>userid</th>
                    <th>delete</th>


                </tr>
            </thead>
            <tbody>
                <?php
                include 'datasource.php';
                $ds = new Database();
                $ds->set_database("todo_db");
                if ($ds->dbconnect()) {

                    $return = $ds->get_users();


                    $total = count($return);


                    $returnlist = "";
                    for ($j = 0; $j < $total; $j++) {


                        //print "<tr class=".$class."><td>" . ($j + 1) . "";                                    
                        print "<tr id=" . "id" . ($j + 1) . "><td>" . $return[$j][0] . "";
                        print "</td><td style=display:none;>" . "button" . "";
                        print "</td><td style=display:none;>" . $return[$j][1] . "";
                        print "</td><td class=" . "dont-touch-me" . "><button class=\"panel-button btn \" data-panelid=" . $return[$j][1] . " data-panelid2=#id" . ($j + 1) . "><i class=\"glyphicon glyphicon-remove\"></i></button>";
                        print "</td></tr>";
                    }
                }



                print "</tbody></table>";
                ?>


                </div>



            <div class="modal fade" id="taskmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">      
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Tasks</h4>
                        </div>
                        <div class="modal-body">

                            <div class="container">


                                <table class="table table-bordered table-hover responsive" id="tasktable">
                                    <thead>
                                        <tr>

                                            <th>priority</th>
                                            <th>taskname</th>
                                            <th>deadline</th>
                                            <th>delete</th>



                                        </tr>
                                    </thead>
                                    <tbody id="tasktablebody">

                                    </tbody>
                                </table>


                            </div>
                            <form role="form" id="formadmin2" name="formadmin2"  method="POST"> 

                                <div style="display:none;" id="userid">S</div>

                                <label>New task</label>
                                <br>
                                <div class="form-group">
                                    <div class="col-md-8">

                                        <select class="form-control input-md" id="taskpriority" name="taskpriority">

                                            <?php
                                            if ($ds->dbconnect()) {
                                                $return = $ds->get_priorities();
                                                $total = count($return);
                                                $returnlist = "";
                                                for ($j = 0; $j < $total; $j++) {
                                                    print "<option value=" . $return[$j][1] . ">" . $return[$j][0] . "</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Taskname" id="taskname">
                                </div>

                                <div class="col-md-4">
                                    <div class="input-group date form_datetime" data-date="<?php $edate = date('Y-m-d 05:55:00', strtotime("0 day")); ?>" data-date-format="yyyy-mm-dd hh:ii:ss" data-link-field="dtp_input1">
                                        <input class="form-control" id="taskdeadline" size="16" type="text"  name="taskdeadline" placeholder="YYYY-MM-DD HH:MM:SS" value="<?php echo isset($_POST['start']) ? $_POST['start'] : '' ?>">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>

                                    </div>
                                </div>
                        </div>



                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success" name="send2">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>



            <script type="text/javascript">

                $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});

                $("body").delegate(".form_datetime", "focusin", function () {
                    $(this).datetimepicker();
                });


            </script>

            <script src="https://code.jquery.com/jquery.js"></script>
            <script src="js/bootstrap.min.js"></script>
            </body>
            </html>