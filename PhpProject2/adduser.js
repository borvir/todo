$(document).ready(function () {

    $('.panel-button').on('click', function () {
        var userid = $(this).attr('data-panelid');
        var trid = $(this).attr('data-panelid2');
        console.log(userid);
        console.log(trid);


        $('#wrong').remove();


        var deletedata = {
            'userid': userid

        };

        // process the form
        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: 'deleteuser.php', // the url where we want to POST
            data: deletedata, // our data object
            dataType: 'json' // what type of data do we expect back from the server
                    //encode: true
        })
                // using the done promise callback
                .done(function (data) {

                    // log data to the console so we can see
                    console.log(data);

                    // here we will handle errors and validation messages
                    if (!data.success) {

                        // handle errors for portal ---------------
                        if (data.errors.begin) {

                            $('#box-content').addClass('error');
                            $('#box-content').append('<div id="wrong" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert">&times;</a><strong>Error! </strong>' + data.errors.begin + '</div>');
                            $('#wrong').fadeOut(4000);


                        }

                        // handle errors for feeder ---------------

                    } else {


                        $('#box-content').append('<div id="success" class="alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a>' + data.admin1 + '</div>');
                        $('#success').fadeOut(3000);


                        $(trid).remove('');



                    }
                })

                // using the fail promise callback
                .fail(function (data) {

                    // show any errors
                    // best to remove for production
                    console.log(data);
                });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });
    $('.panel-button2').on('click', function () {
        var taskid = $(this).attr('data-panelidt');
        var trid2 = $(this).attr('data-panelidt2');
        console.log(taskid);
        console.log(trid2);



        $('#wrong').remove();


        var deletedata = {
            'taskid': taskid

        };

        // process the form
        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: 'deletetask.php', // the url where we want to POST
            data: deletedata, // our data object
            dataType: 'json' // what type of data do we expect back from the server
                    //encode: true
        })
                // using the done promise callback
                .done(function (data) {

                    // log data to the console so we can see
                    console.log(data);

                    // here we will handle errors and validation messages
                    if (!data.success) {

                        // handle errors for portal ---------------
                        if (data.errors.begin) {

                            $('#box-content').addClass('error');
                            $('#box-content').append('<div id="wrong" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert">&times;</a><strong>Error! </strong>' + data.errors.begin + '</div>');
                            $('#wrong').fadeOut(4000);


                        }

                        // handle errors for feeder ---------------

                    } else {


                        $('#modalbox-content').append('<div id="success" class="alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a>' + data.admin1 + '</div>');
                        $('#success').fadeOut(3000);


                        $(trid2).remove('');



                    }
                })

                // using the fail promise callback
                .fail(function (data) {

                    // show any errors
                    // best to remove for production
                    console.log(data);
                });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

    $('#wrong').remove();
    // process the form
    $('#formadmin1').submit(function (event) {

        $('#wrong').remove();

        var formData = {
            'username': $('#username').val()

        };

        // process the form
        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: 'adduser.php', // the url where we want to POST
            data: formData, // our data object
            dataType: 'json', // what type of data do we expect back from the server
            encode: true
        })
                // using the done promise callback
                .done(function (data) {

                    // log data to the console so we can see
                    console.log(data);

                    // here we will handle errors and validation messages
                    if (!data.success) {

                        // handle errors for portal ---------------
                        if (data.errors.begin) {

                            $('#box-content').addClass('error');
                            $('#box-content').append('<div id="wrong" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert">&times;</a><strong>Error! </strong>' + data.errors.begin + '</div>');
                            $('#wrong').fadeOut(4000);

                        }

                        // handle errors for feeder ---------------

                    } else {


                        $('#box-content').append('<div id="success" class="alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a>' + data.admin1 + '</div>');
                        $('#success').fadeOut(3000);


                        $('username').val('');

                        window.location.reload();

                    }
                })

                // using the fail promise callback
                .fail(function (data) {

                    // show any errors
                    // best to remove for production
                    console.log(data);
                });

        // stop the form from submitting the normal way and refreshing the page



        event.preventDefault();

    });

    $('#formadmin2').submit(function (event) {

        $('#wrong').remove();

        var formData2 = {
            'userid': $('#userid').val(),
            'taskname': $('#taskname').val(),
            'taskpriority': $('#taskpriority').val(),
            'taskdeadline': $('#taskdeadline').val()

        };

        // process the form
        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: 'addtask.php', // the url where we want to POST
            data: formData2, // our data object
            dataType: 'json', // what type of data do we expect back from the server
            //encode: true
        })
                // using the done promise callback
                .done(function (data) {

                    // log data to the console so we can see
                    console.log(data);

                    // here we will handle errors and validation messages
                    if (!data.success) {

                        // handle errors for portal ---------------
                        if (data.errors.begin) {

                            $('#box-content').addClass('error');
                            $('#box-content').append('<div id="wrong" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert">&times;</a><strong>Error! </strong>' + data.errors.begin + '</div>');
                            $('#wrong').fadeOut(4000);

                        }

                        // handle errors for feeder ---------------

                    } else {


                        $('#box-content').append('<div id="success" class="alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a>' + data.admin1 + '</div>');
                        $('#success').fadeOut(3000);


                        $('userid').val('');
                        $('taskpriority').val('');
                        $('taskname').val('');
                        $('taskdeadline').val('');

                        window.location.reload();

                    }
                })

                // using the fail promise callback
                .fail(function (data) {

                    // show any errors
                    // best to remove for production
                    console.log(data);
                });

        // stop the form from submitting the normal way and refreshing the page



        event.preventDefault();

    });

    $('#usertable > tbody > tr > td:not(.dont-touch-me)').on('click', function () {
        $('#tasktablebody').empty();
        $("#userid").val($(this).closest('tr').children()[2].textContent);
        var userid = $("#userid").val();
        $.ajax({
            url: "passuserid.php",
            method: "post",
            data: {userid: userid},
            success: function (data)
            {
                $('#tasktablebody').append(data);
                $('#taskmodal').modal("show");
            }
        });


    });

});


