<?php

ini_set('session.save_path', realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/session'));
session_start();

include 'datasource.php';

sleep(0.9);
$errors = array();   // array to hold validation errors
$data = array();   // array to pass back data

if (empty($_POST['taskid'])) {
    $errors['begin'] = 'No taskid';
}

if (!empty($errors)) {

    $data['success'] = false;
    $data['errors'] = $errors;
} else {
    $ds = new Database;

    if ($ds->dbconnect()) {

        $ds->delete_task($_POST['taskid']);

        $data['success'] = true;
        $data['admin1'] = "Törlés Sikerült!";
    } else {
        $errors['begin'] = "DataBase Error!";
        $data['success'] = false;
        $data['errors'] = $errors;
    }
}

// return all our data to an AJAX call
echo json_encode($data);
?>
