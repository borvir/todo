

<?php
ini_set('session.save_path',realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/session'));
session_start();


include 'datasource.php';

sleep(0.9);
$errors = array();   // array to hold validation errors
$data = array();   // array to pass back data

if (empty($_POST['username']) ) {
    $errors['begin'] = 'No username';
}

if (!empty($errors)) {

    $data['success'] = false;
    $data['errors'] = $errors;
} else {
    $ds = new Database;

    if ($ds->dbconnect()) {

            date_default_timezone_set('Europe/Budapest');
            $insertdate = date('Y-m-d H:i:s');
        
            
            $ds->add_user($_POST['username']);
         
            $data['success'] = true;
            $data['admin1'] = "Felvétel Sikerült!";

        
 
        
    } else {
        $errors['begin'] = "DataBase Error!";
        $data['success'] = false;
        $data['errors'] = $errors;
    }
}

echo json_encode($data);
?>
