<?php

ini_set('display_errors', 1);

class Database {

    private $conn;
    private $host = "localhost";
    private $user = "root";
    private $password = "mypass";
    private $database = 'todo_db';
    

    /*

      public $productid;
      public $recnr;
      private $conn;
      private $host = "127.0.0.1";
      private $user = "root";
      private $password = "";
      private $database = "test";

     */

    function __construct() {
        
    }
    
    function dbconnect() {

        try {
            $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->database . '', '' . $this->user . '', '' . $this->password . '');

            return true;
        } catch (Exception $e) {
            return false;
        }
    }


    function set_database($database) {
        $this->database = $database;
    }



     function get_users() {
         $stmt = $this->conn->prepare("SET NAMES UTF8");
$stmt = $this->conn->prepare("SELECT username, userid FROM todo_db.users");

        $stmt->execute();

        //return utf8_encode($row[0]);

        $list = array();
        // van talĂ¡lat
        if ($stmt->rowCount() > 0) {
            while ($row = $stmt->fetch(PDO::FETCH_NUM)) {

                $bindResults = array($row[0], $row[1]);
                array_push($list, $bindResults);
            }
        }

        return $list;
    }
    
         function get_priorities() {
         $stmt = $this->conn->prepare("SET NAMES UTF8");
$stmt = $this->conn->prepare("SELECT priorityname, priorityid FROM todo_db.priority
order by priorityid");

        $stmt->execute();

        //return utf8_encode($row[0]);

        $list = array();
        // van talĂ¡lat
        if ($stmt->rowCount() > 0) {
            while ($row = $stmt->fetch(PDO::FETCH_NUM)) {

                $bindResults = array($row[0], $row[1]);
                array_push($list, $bindResults);
            }
        }

        return $list;
    }
    
      function get_user_task($userid) {
          $stmt = $this->conn->prepare("SET NAMES UTF8");
$stmt = $this->conn->prepare("SELECT priorityname, taskname, deadline, tasks.taskid FROM todo_db.tasks
inner join users on tasks.userid=users.userid
inner join priority on tasks.priorityid=priority.priorityid
where tasks.userid = ?
order by tasks.priorityid desc");
        $stmt->bindValue(1, $userid);
        $stmt->execute();

        //return utf8_encode($row[0]);

        $list = array();
        // van talĂ¡lat
        if ($stmt->rowCount() > 0) {
            while ($row = $stmt->fetch(PDO::FETCH_NUM)) {

                $bindResults = array($row[0], $row[1], $row[2], $row[3]);
                array_push($list, $bindResults);
            }
        }

        return $list;
    }
    
    function add_user($username) {
         $stmt = $this->conn->prepare("SET NAMES UTF8");
        $stmt->execute();
        $stmt = $this->conn->prepare("INSERT INTO `todo_db`.`users` (`username`) VALUES (?);");
        $stmt->bindValue(1, $username);
        
        $stmt->execute();
        
    }
    
        function add_task($userid, $priorityid, $taskname, $deadline) {
         $stmt = $this->conn->prepare("SET NAMES UTF8");
        $stmt->execute();
        $stmt = $this->conn->prepare("INSERT INTO `todo_db`.`tasks` (`userid`, `priorityid`, `taskname`, `deadline`) VALUES (?, ?, ?, ?);");
        $stmt->bindValue(1, $userid);
        $stmt->bindValue(2, $priorityid);
        $stmt->bindValue(3, $taskname);
        $stmt->bindValue(4, $deadline);
        
        $stmt->execute();
        
    }
    
    function delete_user($userid) {
        $stmt = $this->conn->prepare("DELETE FROM `todo_db`.`users` WHERE (`userid` = ?);");
        $stmt->bindValue(1, $userid);

        $stmt->execute();
    }
    
    function delete_task($taskid) {
            $stmt = $this->conn->prepare("DELETE FROM `todo_db`.`tasks` WHERE (`taskid` = ?);");
            $stmt->bindValue(1, $taskid);

            $stmt->execute();
        }
    
}
?>

